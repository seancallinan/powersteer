# powersteer

[![NPM](https://nodei.co/npm/powersteer.png?downloads=true&stars=true)](https://nodei.co/npm/powersteer/)

## About

Powersteer lets you communicate with Transmission.

It provides access to the entire Transmission API and performs checks on arguments before sending anything to transmission-daemon.


## Creating a new client

Require Powersteer and create a new client:

```js
var Powersteer = require('powersteer');

var rpc = new Powersteer({
    url: 'http://my.box.ip:9091/transmission/rpc',
    username: 'mysides',
    password: 'theyaregone'
});
```

The constructor accepts only the following arguments:

### `url` (mandatory)

Self-explanatory enough, but make sure you don't simply give the ip and port of your box; Transmission listens for RPC calls on a specific HTTP address. If we assume the Transmission instance to be installed locally, the default address is 'http://127.0.0.1:9091/transmission/rpc'.

### `username` (optional)

If you ave secured your Transmission daemon, put your username here.

### `password` (mandatory if username not null)

If you have provided a username, the password becomes mandatory. The constructor will throw if you don't provide one.

This field must be in plaintext.


## Using a client

WIP - refer to transmission's docs. These are the methods available in Powersteer with the respective Transmission procedures:

```js
torrentStart - 'torrent-start',
torrentStartNow - 'torrent-start-now',
torrentStop - 'torrent-stop',
torrentVerify - 'torrent-verify',
torrentReannounce - 'torrent-reannounce',
torrentSet - 'torrent-set',
torrentGet - 'torrent-get',
torrentAdd - 'torrent-add',
torrentRemove - 'torrent-remove',
torrentSetLocation - 'torrent-set-location',
torrentRenamePath - 'torrent-rename-path',
sessionSet - 'session-set',
sessionGet - 'session-get',
sessionStats - 'session-stats',
blocklistUpdate - 'blocklist-update',
portTest - 'port-test',
sessionClose - 'session-close',
queueMoveTop - 'queue-move-top',
queueMoveUp - 'queue-move-up',
queueMoveDown - 'queue-move-down',
queueMoveBottom - 'queue-move-bottom',
freeSpace - 'free-space'
```

## Example

```js
var R = require('ramda'),
	Powersteer = require('powersteer');

var trace = (x) => {console.log('TRACE: ', R.toString(x)); return x;};

var rpc = new Powersteer({url: 'http://my.box.ip:9091/transmission/rpc'});

rpc.torrentGet({fields: ['id', 'name', 'percentDone']}).then(trace).catch(trace);

rpc.torrentAdd({filename: 'http://cdimage.debian.org/debian-cd/8.2.0/amd64/bt-cd/debian-8.2.0-amd64-CD-1.iso.torrent', paused: true}).then(trace).catch(trace);

```
