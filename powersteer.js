var R = require('ramda'),
	request = require('request'),
	Promise = require('bluebird'),
	froid = require('froid'),
	tools = require('egeria-tools');

var torrentGetFields = ['activityDate', 'addedDate', 'bandwidthPriority', 'comment', 'corruptEver', 'creator', 'dateCreated', 'desiredAvailable', 'doneDate',
	'downloadDir', 'downloadedEver', 'downloadLimit', 'downloadLimited', 'error', 'errorString', 'eta', 'etaIdle', 'files', 'fileStats', 'hashString',
	'haveUnchecked', 'haveValid', 'honorsSessionLimits', 'id', 'isFinished', 'isPrivate', 'isStalled', 'leftUntilDone', 'magnetLink', 'manualAnnounceTime',
	'maxConnectedPeers', 'metadataPercentComplete', 'name', 'peer-limit', 'peers', 'peersConnected', 'peersFrom', 'peersGettingFromUs', 'peersSendingToUs',
	'percentDone', 'pieces', 'pieceCount', 'pieceSize', 'priorities', 'queuePosition', 'rateDownload', 'rateUpload', 'recheckProgress', 'secondsDownloading',
	'secondsSeeding', 'seedIdleLimit', 'seedIdleMode', 'seedRatioLimit', 'seedRatioMode', 'sizeWhenDone', 'startDate', 'status', 'trackers', 'trackerStats',
	'totalSize', 'torrentFile', 'uploadedEver', 'uploadLimit', 'uploadLimited', 'uploadRatio', 'wanted', 'webseeds', 'webseedsSendingToUs'
];

var torrentSetArgs = {'bandwidthPriority': Number, 'downloadLimit': Number, 'downloadLimited': Boolean, 'files-wanted': Array, 'files-unwanted': Array,
	'honorsSessionLimits': Boolean, 'ids': Array, 'location': String, 'peer-limit': Number, 'priority-high': Array, 'priority-low': Array,
	'priority-normal': Array, 'queuePosition': Number, 'seedIdleLimit': Number, 'seedIdleMode': Number, 'seedRatioLimit': Number, 'seedRatioMode': Number,
	'trackerAdd': Array, 'trackerRemove': Array, 'trackerReplace': Array, 'uploadLimit': Number, 'uploadLimited': Boolean
};

var torrentAddArgs = {'cookies': String, 'download-dir': String, 'filename': String, 'metainfo': String, 'paused': Boolean, 'peer-limit': Number, 'bandwidthPriority': Number,
	'files-wanted': Array, 'files-unwanted': Array, 'priority-high': Array, 'priority-low': Array, 'priority-normal': Array};

var sessionSetArgs = {'alt-speed-down': Number, 'alt-speed-enabled': Boolean, 'alt-speed-time-begin': Number, 'alt-speed-time-enabled': Boolean, 'alt-speed-time-end': Number,
	'alt-speed-time-day': Number, 'alt-speed-up': Number, 'blocklist-url': String, 'blocklist-enabled': Boolean, 'blocklist-size': Number, 'cache-size-mb': Number,
	'config-dir': String, 'download-dir': String, 'download-queue-size': Number, 'download-queue-enabled': Boolean, 'dht-enabled': Boolean, 'encryption': String,
	'idle-seeding-limit': Number, 'idle-seeding-limit-enabled': Boolean, 'incomplete-dir': String, 'incomplete-dir-enabled': Boolean, 'lpd-enabled': Boolean,
	'peer-limit-global': Number, 'peer-limit-per-torrent': Number, 'pex-enabled': Boolean, 'peer-port': Number, 'peer-port-random-on-start': Boolean,
	'port-forwarding-enabled': Boolean, 'queue-stalled-enabled': Boolean, 'queue-stalled-minutes': Number, 'rename-partial-files': Boolean, 'rpc-version': Number,
	'rpc-version-minimum': Number, 'script-torrent-done-filename': String, 'script-torrent-done-enabled': Boolean, 'seedRatioLimit': Number, 'seedRatioLimited': Boolean,
	'seed-queue-size': Number, 'seed-queue-enabled': Boolean, 'speed-limit-down': Number, 'speed-limit-down-enabled': Boolean, 'speed-limit-up': Number,
	'speed-limit-up-enabled': Boolean, 'start-added-torrents': Boolean, 'trash-original-torrent-files': Boolean, 'utp-enabled': Boolean, 'version': String
};


var apiMap = {
	sessionField: 'x-transmission-session-id',
	methods: {
		torrentStart: 'torrent-start',
		torrentStartNow: 'torrent-start-now',
		torrentStop: 'torrent-stop',
		torrentVerify: 'torrent-verify',
		torrentReannounce: 'torrent-reannounce',
		torrentSet: 'torrent-set',
		torrentGet: 'torrent-get',
		torrentAdd: 'torrent-add',
		torrentRemove: 'torrent-remove',
		torrentSetLocation: 'torrent-set-location',
		torrentRenamePath: 'torrent-rename-path',
		sessionSet: 'session-set',
		sessionGet: 'session-get',
		sessionStats: 'session-stats',
		blocklistUpdate: 'blocklist-update',
		portTest: 'port-test',
		sessionClose: 'session-close',
		queueMoveTop: 'queue-move-top',
		queueMoveUp: 'queue-move-up',
		queueMoveDown: 'queue-move-down',
		queueMoveBottom: 'queue-move-bottom',
		freeSpace: 'free-space'
	},
	sanity: {
		torrentGet: {
			fields: { type: Array, anyOf: torrentGetFields, mandatory: true },
            ids: { Array }
		},
		torrentSet: R.mapObjIndexed((type) => ({type: type}), torrentSetArgs),
		torrentAdd: R.mapObjIndexed((type) => ({type: type}), torrentAddArgs),
		sessionSet: R.merge(
            R.mapObjIndexed((type) => ({type: type}), sessionSetArgs),
			{ allowed: R.concat(R.keys(sessionSetArgs, ['units'])) }
        ),
		torrentRemove: {
            ids: { type: Array },
            'delete-local-data': { type: Boolean }
		},
		torrentSetLocation: {
            ids: { type: Array },
            location: {type: String},
            move: {type: Boolean}
		},
		torrentRenamePath: {
            ids: { type: Array },
            path: { type: String },
            name: { type: String }
		},
		freeSpace: {
            path: { type: String, mandatory: true }
        }
	}
};

//calls that expect no arguments
var _sanity_none_allowed = {allowed: []};
R.forEach((call) => apiMap.sanity[call] = _sanity_none_allowed, ['sessionGet', 'sessionStats', 'sessionClose', 'blocklistUpdate', 'portTest']);

//calls that only expect ids
var _sanity_ids_allowed = {id: {type: Array}, allowed: ['ids']};
R.forEach((call) => apiMap.sanity[call] = _sanity_ids_allowed, ['torrentStart', 'torrentStartNow', 'torrentStop', 'torrentVerify', 'torrentReannounce']);

//queue operations
var _sanity_only_ids_required = { ids: {type: Array, mandatory: true }, allowed: ['ids'] };
R.forEach((call) => apiMap.sanity[call] = _sanity_only_ids_required, ['queueMoveTop', 'queueMoveUp', 'queueMoveDown', 'queueMoveBottom']);

//allowed same as type
R.forEach((call) => apiMap.sanity[call].allowed = R.keys(apiMap.sanity[call].type), ['torrentGet', 'torrentSet', 'torrentAdd', 'torrentRemove', 'torrentSetLocation', 'torrentRenamePath']);


const err_prefix = 'Powersteer | ERR |';

const errors = {
	ConfigurationError: tools.customError('ConfigurationError', 'Configuration error'),
	TransmissionAPIError: tools.customError('TransmissionAPIError', 'Transmission API error: illegal call'),
	TransmissionOperationError: tools.customError('TransmissionOperationError', 'API call succeeded, but operation was unsuccessful'),
};


var query = function(obj, call, req, resolve, reject) {
	request.post(req, (err, res) => {
		if (err) {
			reject(new errors.TransmissionAPIError(err_prefix, null, err));
		} else {
			if (res.headers[apiMap.sessionField]) {
				obj.headers[apiMap.sessionField] = req.headers[apiMap.sessionField] = res.headers[apiMap.sessionField];
			}
			switch (res.statusCode) {
				case 409: // wrong session
					query(obj, call, req, resolve, reject); //session ID has been updated, so just repeat the call
					break;

				case 401:
					reject(new errors.TransmissionAPIError(err_prefix, '401: Invalid username/password'));
					break;

				default:
					try {
						var jsonBody = JSON.parse(res.body);
					} catch (e) {
						reject(new errors.TransmissionAPIError(err_prefix, (call ? 'API call succeeded, but operation ' + call + ' was unsuccessful' : null), e));
					}

					if (jsonBody.result !== 'success') {
						reject(new errors.TransmissionOperationError(err_prefix, null, jsonBody));
					} else {
						resolve(jsonBody);
					}
					break;
			};
		}
	});
};

var apiCall = function(obj, call, args) {
	var payload = JSON.stringify({
		'method': call,
		'arguments': args
	}),
	req = {uri: obj.url, body: payload, headers: obj.headers};
	return new Promise((resolve, reject) => {
		query(obj, call, req, resolve, reject);
	});

};


var Powersteer = function(options) {
	var config = options || {};

    var sanity = {
        type: {url: String, username: String, password: String},
        complexType: {url: 'URI'},
        mandatory: {url: true}
    };

    if (!R.isNil(config.username)) {
        sanity.mandatory.username = true;
        sanity.mandatory.password = true;
    }

	var errorMsg = froid(sanity, config);
	if (!R.isNil(errorMsg)) throw errors.ConfigurationError(err_prefix, errorMsg);

	this.url = config.url;
	this.headers = {'Content-Type': 'application/json'};
	if (!R.isNil(config.username)) {
		this.username = config.username;
		this.password = config.password;
		var auth = new Buffer(this.username + ':' + this.password).toString('base64');
		this.headers['Authorization'] = 'Basic ' + auth;
	}
};

Powersteer.prototype = R.mapObjIndexed((call, method) => {
	return function(args) {
		var options = args || {};

		var errorMsg = froid(R.dissoc('allowed', apiMap.sanity[method]), options);
		if (!R.isNil(errorMsg)) throw new errors.TransmissionAPIError(err_prefix, errorMsg);

		var exclude = R.omit(apiMap.sanity[method].allowed, options);
		options = R.omit(R.keys(exclude), options);

		return apiCall(this, call, args);
	}
}, apiMap.methods);

Powersteer.errors = errors;


module.exports = Powersteer;
